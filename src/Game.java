import java.util.InputMismatchException;
import java.util.Scanner;

public class Game {

    private Scanner sc = new Scanner(System.in);
    private int row;
    private int col;
    Board board = null;
    Player o = null;
    Player x = null;

    public Game() {
        this.o = new Player('o');
        this.x = new Player('x');
    }
    public void run() {
        while(true) {
            this.runOnce();
            if(!askContinue()) {
                return;
            }
        }
    }
    private int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }
    public void newGame() {
        if(getRandomNumber(1,100)%2==0) {
            this.board = new Board(o, x);
        } else {
            this.board = new Board(x, o);
        }
        
    }
    public void runOnce() {
        this.showWelcome();
        this.newGame();
        while(true) {
            this.showTable();
            this.showTurn();
            this.inputRowCol();
            if(board.checkWin()) {
                this.showResult();
                this.showStat();
                return;
            }
            board.switchPlayer();
        }

    }

    private void showResult() {
        if(board.getWinner() != null) {
            showWin();
        } else {
            showDraw();
        }
    }

    private void showDraw() {
        System.out.println("Draw!!!!");
    }

    private void showWin() {
        System.out.println(board.getWinner().getName() + " Win!!!!");
    }

    private void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private void showTable() {
        char[][] data = this.board.getData();
        for(int row=0; row<data.length; row++) {
            System.out.print("|");
            for(int col=0; col<data[row].length; col++) {
                System.out.print(" " + data[row][col] + " |");
            }
            System.out.println("");
        }
    }

    private void showTurn() {
        System.out.println(board.getCurrentPlayer().getName() + " Turn");
    }

    private void input() {
        while(true) {
            try {
                System.out.print("Please input row col: ");
                this.row = sc.nextInt();
                this.col = sc.nextInt();
                return;
            } catch(InputMismatchException iE) {
                sc.next();
                System.out.println("Please input number 1-3!!");
            }
        }  
    }

    private void inputRowCol() {
        while(true) {
            this.input();
            try {
                if(board.setRowCol(row, col)) {
                    System.out.println("Fuck");
                    return;
                } 
            } catch(ArrayIndexOutOfBoundsException e) {
                System.out.println("Please input number 1-3!!");
            }
             
        }
    }

    private boolean askContinue() {
        while(true) {
            System.out.print("Continue  Y/n ? :");
            String ans = sc.next();
            if(ans.equals("n")) {
                return false;
            } else if(ans.equals("Y")) {
                return true;
            }   
        }
    }

    private void showStat() {
        System.out.println(o.getName() + " (win,lose,draw) : " + o.getWin() + ", " + o.getLose() + ", " + o.getDraw());
        System.out.println(x.getName() + " (win,lose,draw) : " + x.getWin() + ", " + x.getLose() + ", " + x.getDraw());
    }

}
