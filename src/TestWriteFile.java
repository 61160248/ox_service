
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author user
 */
public class TestWriteFile {

    public static void main(String[] args) {

        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        Player o = new Player('O');
        Player x = new Player('X');
        o.countLose();
        x.countWin();
        o.countDraw();
        x.countDraw();
        o.countWin();
        x.countLose();
        try {
            file = new File("FileOX.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(o);
            oos.writeObject(x);
        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {

        } finally {
            try {
                if(fos != null) fos.close();
                if(oos != null) oos.close();
            } catch (IOException ex) {
                
            }
        }
    }
}
